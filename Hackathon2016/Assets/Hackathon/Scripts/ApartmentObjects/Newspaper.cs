﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackathon2016
{
    // updateable that changes material albedo to a new newspaper textures
    public class Newspaper : MonoBehaviour, IUpdateable
    {
        [SerializeField] MeshRenderer       m_Renderer;
        private int                         m_CurrTexture = 0;
        [SerializeField] List<Texture>      m_Textures = new List<Texture>();

        public void Start()
        {
            m_Renderer.material.mainTexture = m_Textures[m_CurrTexture];
        }

        public void Renew()
        {
            m_CurrTexture++;
            if(m_CurrTexture >= m_Textures.Count)
            {
                m_CurrTexture = 0;
            }

            m_Renderer.material.mainTexture = m_Textures[m_CurrTexture];
        }
    }
}
