﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackathon2016
{
    public class Plant : MonoBehaviour, IInteractable, IUpdateable
    {
        private Transform m_Transform;
        private PlantArea m_Parent;
        private float m_CurrScale;
        private bool m_Completed = false;
        [SerializeField] private float m_StartScale = 0.1f;        
        [SerializeField] private float m_EndScale = 1.0f;

        public void Start()
        {
            m_CurrScale = m_StartScale;
            m_Completed = false;
            m_Transform = transform;
            m_Transform.localScale = Vector3.one * m_CurrScale;
            m_Parent = GetComponentInParent<PlantArea>();
        }

        public void Interact()
        {
            if (m_Completed || PlayerHoldObject.IsHolding)
                return;

            PlayerWaterCan.Instance.IsWatering(true);

            m_Transform.localScale = Vector3.one * m_CurrScale;
            m_CurrScale += Time.deltaTime;
            
            if(m_CurrScale >= m_EndScale)
            {
                m_Completed = true;
                m_Parent.PlantFinished();
            }
        }

        public void Renew()
        {
            m_Completed = false;
            m_CurrScale = m_StartScale;
            m_Transform.localScale = Vector3.one * m_StartScale;
        }
    }
}
