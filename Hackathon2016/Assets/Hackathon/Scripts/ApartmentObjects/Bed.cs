﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Hackathon2016
{// calls updateables
    public class Bed : MonoBehaviour, IInteractable
    {
        private List<GameObject> m_Updateables = new List<GameObject>();
        [SerializeField] private Image m_SleepImage;

        private void Start()
        {
            m_Updateables = GameObject.FindGameObjectsWithTag("Updateable").ToList();
            m_SleepImage.enabled = false;
        }

        public void Interact()
        {
            PlayerInteract.CanInteract = false;
            UnityStandardAssets.Characters.FirstPerson.FirstPersonController.CanMove = false;
            PlayerWaterCan.Instance.HideCan(true);

            StartCoroutine(Sleepytime());
        }

        private IEnumerator Sleepytime()
        {
            m_SleepImage.enabled = true;

            float lerp = 0.0f;
            while(lerp < 1.0f)
            {
                yield return new WaitForEndOfFrame();
                lerp += Time.deltaTime;

                Color copy = m_SleepImage.color;
                copy.a = lerp;
                m_SleepImage.color = copy;
            }

            for (int loop = 0; loop < m_Updateables.Count(); loop++)
                m_Updateables[loop].SendMessage("Renew");

            while (lerp > 0.0f)
            {
                yield return new WaitForEndOfFrame();
                lerp -= Time.deltaTime;

                Color copy = m_SleepImage.color;
                copy.a = lerp;
                m_SleepImage.color = copy;
            }
            
            m_SleepImage.enabled = false;
            PlayerWaterCan.Instance.HideCan(false);
            PlayerInteract.CanInteract = true;
            UnityStandardAssets.Characters.FirstPerson.FirstPersonController.CanMove = true;
        }
    }
}
