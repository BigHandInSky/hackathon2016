﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackathon2016
{
    public class PlayerHoldObject : MonoBehaviour
    {
        private static PlayerHoldObject m_Instance;
        public static PlayerHoldObject Instance { get { return m_Instance; } }

        public static bool                  IsHolding = false;
        [SerializeField] private Transform  m_HandHoldTransform;
        private Transform                   m_HoldObject = null;
        private bool                        m_IsHoldingFruit = false;

        void Awake()
        {
            if (m_Instance)
            {
                if (m_Instance != null)
                {
                    Destroy(gameObject);
                    return;
                }
            }

            m_Instance = this;
        }

        public void AssignObject(Transform obj)
        {
            m_HoldObject = obj;

            m_HoldObject.SetParent(m_HandHoldTransform);
            //m_HoldObject.localScale = Vector3.one;
            m_HoldObject.localPosition = Vector3.zero;
            m_HoldObject.localRotation = Quaternion.Euler(Vector3.zero);

            PlayerWaterCan.Instance.HideCan(true);
            IsHolding = true;
        }

        public Transform DropOff()
        {
            PlayerWaterCan.Instance.HideCan(false);
            IsHolding = false;

            Destroy(m_HoldObject.gameObject);
            m_HoldObject = null;
            return m_HoldObject;
        }
    }
}
