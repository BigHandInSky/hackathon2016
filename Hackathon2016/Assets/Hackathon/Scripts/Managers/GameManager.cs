﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace Hackathon2016
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager                      m_Instance;
        public static GameManager                       Instance { get { return m_Instance; } }
        
        [SerializeField] private GameObject             m_GameCanvas;
        [SerializeField] private MainMenuFunctions      m_MenuFuncs;

        void Awake()
        {
            if (m_Instance)
            {
                if (m_Instance != null)
                {
                    Destroy(gameObject);
                    return;
                }
            }

            m_Instance = this;

            FirstPersonController.CanMove = false;
            PlayerInteract.CanInteract = false;
            PlayerWaterCan.Instance.HideCan(true);
            Cursor.lockState = CursorLockMode.Confined;            
        }

        public void SwitchToGame()
        {
            FirstPersonController.CanMove = true;
            PlayerInteract.CanInteract = true;
            PlayerWaterCan.Instance.HideCan(false);
            Cursor.visible = false;

            m_GameCanvas.SetActive(true);
        }
        public void SwitchToMenu()
        {
            m_GameCanvas.SetActive(false);
            m_MenuFuncs.Start();

            FirstPersonController.CanMove = false;
            PlayerInteract.CanInteract = false;
            PlayerWaterCan.Instance.HideCan(true);
            Cursor.visible = true;
        }
    }
}