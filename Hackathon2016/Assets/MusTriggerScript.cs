﻿using UnityEngine;
using System.Collections;
using Hackathon2016;

public class MusTriggerScript : MonoBehaviour {

	[SerializeField] private MusicElement FadeInOnEnter;
	[SerializeField] private MusicElement FadeOutOnEnter;

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Player") {
			if (FadeInOnEnter != null)
				FadeInOnEnter.FadeIn ();
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.tag == "Player") {
			if (FadeOutOnEnter != null)
				FadeOutOnEnter.FadeOut ();
		}
	}
}
