﻿using UnityEngine;
using System.Collections;

namespace Hackathon2016
{

	public static class AudioSourceExtensions 
	{
		static void FadeOut(this AudioSource source)
		{
			
		}

		private static IEnumerator FadeInAudioSource(AudioSource source, float time, float endValue=1.0f)
		{
			float lerpValue = 0.0f;
			
			while (lerpValue <= 1.0f)
			{
				source.volume = Mathf.Lerp (0.0f, endValue, lerpValue);
				lerpValue += (Time.deltaTime / time);
				yield return new WaitForEndOfFrame ();
			}
		}

		private static IEnumerator FadeOutAudioSource(AudioSource source, float time)
		{
			float lerpValue = 0.0f;
			float startValue = source.volume;

			while (lerpValue <= 1.0f)
			{
				source.volume = Mathf.Lerp (startValue, 0.0f, lerpValue);
				lerpValue += (Time.deltaTime / time);
				yield return new WaitForEndOfFrame ();
			}
		}
	}
}