﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackathon2016
{
    public class FruitDropoff : MonoBehaviour, IInteractable
    {/*
        [SerializeField] private Transform m_DropOffPoint;
        private List<GameObject> m_Fruits = new List<GameObject>();*/
        
        public void Interact()
        {
            if (!PlayerHoldObject.IsHolding)
                return;

            Transform newFruit = PlayerHoldObject.Instance.DropOff();
            /*newFruit.SetParent(m_DropOffPoint);

            newFruit.localScale = Vector3.one;
            newFruit.localPosition = Vector3.zero;
            newFruit.localRotation = Quaternion.Euler(Vector3.zero);

            m_Fruits.Add(newFruit.gameObject);*/
        }
    }
}
