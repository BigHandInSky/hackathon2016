﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hackathon2016
{
	public class MusicElementCommand
	{
		public enum CommandName
		{
			Play,
			Stop
		}

		private CommandName m_CmdName;
		private string m_ElementName;

		public string ElementName { get { return m_ElementName; } }
		public CommandName CmdName { get { return m_CmdName; } } 

		public MusicElementCommand(string nName, CommandName nCmd)
		{
			m_ElementName = nName;
			m_CmdName = nCmd;
		}
	}

	public class DynamicMusic : MonoBehaviour 
	{
		[SerializeField] private double m_BPM;
		[SerializeField] private List<MusicElement> m_MusicList;

		private double m_BeatLengthSeconds;
		private double m_LastClockTime;
		private List<MusicElementCommand> m_CommandList;

		private void Start()
		{
			m_CommandList = new List<MusicElementCommand> ();

			StartClock ();
		}

		private void StartClock()
		{
			m_BeatLengthSeconds = 60 / m_BPM;
			StartCoroutine (DoClock ());
		}

		private IEnumerator DoClock()
		{
			m_LastClockTime = Time.time;

			if (Time.time - m_LastClockTime >= m_BeatLengthSeconds) 
			{
				DoCommands ();
			}

			yield return new WaitForEndOfFrame ();
		}

		public void AddCommand(string musicName, MusicElementCommand.CommandName cmdName)
		{
			m_CommandList.Add (new MusicElementCommand (musicName, cmdName));
		}

		private void DoCommands()
		{
			List<int> deletions = new List<int> ();

			for (int i=0; i < m_CommandList.Count; i++)
			{
				foreach (MusicElement mus in m_MusicList) {
					if (mus.Name == m_CommandList[i].ElementName) {
						if (m_CommandList[i].CmdName == MusicElementCommand.CommandName.Play) {
							mus.Play ();
						}
						if (m_CommandList[i].CmdName == MusicElementCommand.CommandName.Stop)
						{
							mus.Stop ();
						}
						deletions.Add (i);
					}
				}
			}

			foreach (int del in deletions) {
				m_CommandList.RemoveAt (del);
			}
		}
	}
}
