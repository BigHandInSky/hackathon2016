﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackathon2016
{
    public class PlayerInteract : MonoBehaviour        
    {     /*          
        void Start()
        {
            
        }*/

        public static bool CanInteract = true;

        void Update()
        {
            if(Input.GetMouseButton(0) && CanInteract)
            {
                RaycastHit hit;
                if(Physics.Raycast(transform.position, transform.forward, out hit))
                {
                    if(hit.collider.tag == "Interactable")
                    {
                        hit.collider.SendMessage("Interact");
                    }
                }
            }
            else
            {
                PlayerWaterCan.Instance.IsWatering(false);
            }
        }
    }
}
