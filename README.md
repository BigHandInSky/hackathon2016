# README #

This is the repository for the game made by Team Eight-Eyes for the ART // GAMES // HACKATHON at Gamecity Nottingham 2016 (https://www.eventbrite.co.uk/e/artgameshackathon-registration-23570870107), open-sourced in the Gamejam spirit under the following Creative Commons license: http://creativecommons.org/licenses/by-nc-sa/4.0/

### What is this repository for? ###

* ZENDAY, an art-game made for the above Hackathon
* 1.0 Prototype Gamejam game edition

### How do I get set up? ###

This game was made with the following tools, so it's recommended to use these or newer versions of the software.

* Unity 5.3.4f1
* Blender 2.77
* Visual Studio 2015 Community