﻿using UnityEngine;
using System.Collections;

namespace Hackathon2016
{
    public class PlayerToneControl : MonoBehaviour
    {
        [SerializeField]
        private UnityStandardAssets.ImageEffects.BloomOptimized m_Bloom;
        [SerializeField]
        private UnityStandardAssets.ImageEffects.ColorCorrectionCurves m_ColourCorrection;
        
        void Start()
        {
            ChangeSettings(true);
        }

        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
                ChangeSettings(true);
        }

        void OnTriggerExit(Collider coll)
        {
            if (coll.tag == "Player")
                ChangeSettings(false);
        }

        void ChangeSettings(bool entered)
        {
            m_Bloom.intensity = (entered) ? 0.1f : 0.4f;
            m_ColourCorrection.saturation = (entered) ? 0.3f : 1.0f;
        }
    }
}