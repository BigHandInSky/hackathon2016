﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hackathon2016
{
	[RequireComponent(typeof(AudioSource))]
	public class MusicElement : MonoBehaviour
	{
        public static float MasterVolume = 1.0f;

		private const float _FADETIME = 1.0f;

		[SerializeField] private int m_LengthBeats = 4;
		[SerializeField] private double m_BPM = 120;
		[SerializeField] private int m_NumPlays = 1;
		[SerializeField] private int m_OverlapSourceDepth = 2;
		[SerializeField] private string m_Naame = "defaultName";
		[SerializeField] private bool m_LoopIndefinitely;

		private List<AudioSource> m_OverlapSources;
		private AudioSource m_Source;
		public AudioSource Source {get { return m_Source;}}
		public string Name { get { return m_Naame; } }

		private float m_IdealVolume;

		private void Start()
		{
            MasterVolume = 1.0f;

            m_Source = GetComponent<AudioSource>();
			m_Source.loop = false;

			m_OverlapSources = new List<AudioSource> (m_OverlapSourceDepth);

			for(int i=0; i<m_OverlapSourceDepth; i++){
				m_OverlapSources.Add (gameObject.AddComponent<AudioSource> ());
				m_OverlapSources[i].clip = m_Source.clip;
				m_OverlapSources[i].volume = m_Source.volume;
			}

			m_IdealVolume = m_Source.volume;

			StartCoroutine (SchedulePlays ());
		}

		public void FadeOut()
		{
			StartCoroutine (CFadeOut ());
		}

		public void FadeIn()
		{
			StartCoroutine (CFadeIn ());
		}

		private IEnumerator CFadeOut()
		{
			float lerper = 0.0f;
			float startVolume = m_Source.volume;

			while (lerper <= 1.0f) {

				m_Source.volume = Mathf.Lerp (startVolume, 0.0f,lerper);
				UpdateVolumes ();
				lerper += Time.deltaTime / _FADETIME;
				yield return new WaitForEndOfFrame ();
			}
		}

		private IEnumerator CFadeIn()
		{
			float lerper = 0.0f;
			float startVolume = m_Source.volume;

			while (lerper <= 1.0f) {

				m_Source.volume = Mathf.Lerp (0.0f, 1.0f, lerper);
				UpdateVolumes ();
				lerper += Time.deltaTime / _FADETIME;
				yield return new WaitForEndOfFrame ();
			}
		}

		private void UpdateVolumes()
		{
			for (int i = 0; i < m_OverlapSources.Count; i++)
			{
				m_OverlapSources [i].volume = m_Source.volume * MasterVolume;
			}
		}

		IEnumerator SchedulePlays()
		{
			int repeatNum = 1;

			while (repeatNum <= m_NumPlays) {
				
				int beatNum = 1;
				while (beatNum <= m_LengthBeats) {
					Play ();
					beatNum++;
					yield return new WaitForSeconds ((float)(60 / m_BPM) * m_LengthBeats);
				}

				if (!m_LoopIndefinitely) {
					repeatNum++;
				}
				yield return new WaitForEndOfFrame ();
			}
		}

		public void Play()
		{
			PlayOverlap ();
			if (m_Source.isPlaying) {
				PlayOverlap ();
			} 
			else 
			{
				m_Source.Play ();
			}
		}

		public void Stop()
		{
			m_Source.Stop ();
		}

		private void PlayOverlap()
		{
			foreach (AudioSource osource in m_OverlapSources)
			{
				if (!osource.isPlaying) {
					osource.Play ();
					break;
				}
			}
		}
	}
}

