﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Hackathon2016
{
    [RequireComponent(typeof(Slider))]
    public class AudioSlider : MonoBehaviour
    {
        private Slider              m_Slider;
        [SerializeField]
        private MainMenuFunctions   m_FunctionsScript;

        private void Start()
        {
            m_Slider = GetComponent<Slider>();
            m_Slider.onValueChanged.AddListener(x => { SliderAction(); });
        }

        private void SliderAction()
        {
            m_FunctionsScript.ChangeAudioLevel(Mathf.RoundToInt(m_Slider.value));
        }
    }
}