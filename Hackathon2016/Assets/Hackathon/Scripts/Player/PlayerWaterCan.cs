﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hackathon2016
{
    public class PlayerWaterCan : MonoBehaviour
    {
        private static PlayerWaterCan m_Instance;
        public static PlayerWaterCan Instance { get { return m_Instance; } }

        [SerializeField] private Transform m_WateringCanTransform;
        [SerializeField] private Transform m_HandHoldTransform;
        [SerializeField] private Transform m_Watering;

        void Awake()
        {
            if (m_Instance)
            {
                if (m_Instance != null)
                {
                    Destroy(gameObject);
                    return;
                }
            }

            m_Instance = this;
        }

        public void IsWatering(bool value)
        {
            if(value)
            {
                m_WateringCanTransform.localRotation = m_Watering.localRotation;
            }
            else
            {
                m_WateringCanTransform.localRotation = m_HandHoldTransform.localRotation;
            }
        }

        public void HideCan(bool hide)
        {
            if (hide && m_WateringCanTransform.gameObject.activeInHierarchy)
            {
                m_WateringCanTransform.gameObject.SetActive(false);
            }
            else if (!m_WateringCanTransform.gameObject.activeInHierarchy)
            {
                m_WateringCanTransform.gameObject.SetActive(true);
            }
        }
    }
}
