﻿using UnityEngine;
using System.Collections;

namespace Hackathon2016
{
    public class EscapeToMenu : MonoBehaviour
    {
        [SerializeField] private KeyCode m_EscapeKey = KeyCode.Escape;

        void Update()
        {
            if(Input.GetKey(m_EscapeKey))
            {
                GameManager.Instance.SwitchToMenu();
            }
        }
    }
}