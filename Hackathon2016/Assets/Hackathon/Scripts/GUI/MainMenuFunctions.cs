﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Hackathon2016
{
    public class MainMenuFunctions : MonoBehaviour
    {
        [Header("Menu Panels")]
        [SerializeField] private RectTransform  m_MenuPanel;
        [SerializeField] private RectTransform  m_OptionsPanel;
        [SerializeField] private Text           m_GraphicsText;
        [SerializeField] private Text           m_AudioText; 
        private float                           m_WidthShow = 210.0f;
        private float                           m_WidthHide = 0.0f;
        private float                           m_AnimLength = 0.5f;

        public void Start()
        {
            m_OptionsPanel.sizeDelta = new Vector2(m_WidthHide, m_OptionsPanel.sizeDelta.y);
            m_MenuPanel.sizeDelta = new Vector2(m_WidthHide, m_MenuPanel.sizeDelta.y);

            m_MenuPanel.gameObject.SetActive(true);
            m_OptionsPanel.gameObject.SetActive(false);

            StartCoroutine(AnimatePanel(true, m_MenuPanel, 0.5f));
        }
        private IEnumerator AnimatePanel(bool animToShow, RectTransform transf, float delay)
        {
            yield return new WaitForSeconds(delay);

            Vector2 startDelta = transf.sizeDelta;
            Vector2 gotoDelta = transf.sizeDelta;

            if (animToShow)
            {
                /*if (transf.sizeDelta.x == m_WidthShow)
                    yield break;*/

                transf.gameObject.SetActive(true);

                startDelta.x = m_WidthHide;
                gotoDelta.x = m_WidthShow;
            }
            else
            {
                /*if (transf.sizeDelta.x == m_WidthHide)
                    yield break;*/

                startDelta.x = m_WidthShow;
                gotoDelta.x = m_WidthHide;
            }

            transf.sizeDelta = startDelta;

            float lerp = 0.0f;
            while (lerp < 1.0f)
            {
                yield return new WaitForEndOfFrame();
                lerp += Time.deltaTime / m_AnimLength;

                transf.sizeDelta = Vector2.Lerp(startDelta, gotoDelta, lerp);
            }

            transf.sizeDelta = gotoDelta;

            if(!animToShow)
                transf.gameObject.SetActive(false);
        }

        public void StartGame()
        {
            if (GameManager.Instance)
            {
                GameManager.Instance.SwitchToGame();
                SwitchPanel(-1);
            }
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        /* -1   - close all
         * 0    - Menu
         * 1    - Options
         * 2+   - Hell, damnation, sadness
        */
        public void SwitchPanel(int panelIndex)
        {
            switch (panelIndex)
            {
                case (-1):
                    StartCoroutine(AnimatePanel(false, m_MenuPanel, 0.0f));
                    StartCoroutine(AnimatePanel(false, m_OptionsPanel, 0.0f));
                    break;
                case (0):
                    StartCoroutine(AnimatePanel(true, m_MenuPanel, 0.5f));
                    StartCoroutine(AnimatePanel(false, m_OptionsPanel, 0.0f));
                    break;
                case (1):
                    StartCoroutine(AnimatePanel(false, m_MenuPanel, 0.0f));
                    StartCoroutine(AnimatePanel(true, m_OptionsPanel, 0.5f));
                    break;
            }
        }

        public void ChangeGraphicsLevel(bool changeUp)
        {
            if(changeUp)
            {
                QualitySettings.IncreaseLevel(true);
            }
            else
            {
                QualitySettings.DecreaseLevel(true);
            }

            m_GraphicsText.text = "GRAPHICS " + QualitySettings.names[QualitySettings.GetQualityLevel()];
        }

        public void ChangeAudioLevel(int newLevel)
        {
            //AudioMaster.SetLevel(newLevel);
            MusicElement.MasterVolume = Mathf.Clamp((newLevel * 0.01f), 0.0f, 1.0f);

            if (newLevel > 0)
                m_AudioText.text = "AUDIO " + newLevel.ToString();
            else
                m_AudioText.text = "AUDIO OFF";
        }

        public void ChangeResolution(int width, int height)
        {
            Screen.SetResolution(width, height, true);
        }
    }
}