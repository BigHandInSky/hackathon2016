﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Hackathon2016
{
    public class PlantArea : MonoBehaviour, IUpdateable
    {
        [SerializeField] private GameObject     m_PlantPickable;

        public void Start()
        {
        }

        public void PlantFinished()
        {
            GameObject clone = Instantiate(m_PlantPickable, transform.position, transform.rotation) as GameObject;
            PlayerHoldObject.Instance.AssignObject(clone.transform);
        }

        public void Renew()
        {
            for (int loop = 0; loop < transform.childCount; loop++)
                transform.GetChild(loop).SendMessage("Renew", SendMessageOptions.DontRequireReceiver);
        }

    }
}
